/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandandstate;

/**
 *
 * @author ljclaridge
 */
public class Receiver {
    private boolean floatInAir = false;
    private boolean visible = true;
    private boolean isProtected = false;
    private IState state;
    public Receiver()
    {
        
    }
    public Receiver(IState state)
    {
    this.state = state;
    }
        
    public boolean isFloatInAir() {
        return floatInAir;
    }

    public void setFloatInAir(boolean floatInAir) {
        this.floatInAir = floatInAir;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    public boolean isProtected()
    {
        return isProtected;
    }
    
    //State pattern specfic code
    
    public void setState(IState state) {
        this.state = state;
    }

    public IState getState() {
        return state;
    }
    
    public void castProtectionSpell(){
        state.castProtectionSpell(this);
    }
    
    public void printState(){
        this.state.printState();
    }
    
}
