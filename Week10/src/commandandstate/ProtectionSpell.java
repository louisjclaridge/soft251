/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandandstate;

/**
 *
 * @author ljclaridge
 */
public class ProtectionSpell implements ICommand {
    private Receiver receiver;
    public ProtectionSpell(Receiver receiver) {
        this.receiver = receiver;
    }
    
    @Override
    public void execute() {
        receiver.castProtectionSpell();
    }

    @Override
    public void undo() {
        receiver.castProtectionSpell();
    }
        
}
