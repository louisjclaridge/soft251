/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandandstate;

/**
 *
 * @author ljclaridge
 */
public class Protected implements IState {
    @Override
    public void castProtectionSpell(Receiver receiver) {
        System.out.println("Spell Cast! Going to unprotected state!");
        receiver.setState(new Unprotected());
    }

    @Override
    public void toggleProtectionSpell(Receiver receiver) {
        System.out.println("In protected state -- spell can't be cast!");
        receiver.setState(this);
    }

    @Override
    public void printState() {
        System.out.println("I am protected!");
    }
    
}
