/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandandstate;

/**
 *
 * @author ljclaridge
 */
public interface IState {
    public void castProtectionSpell(Receiver Receiver);
    public void toggleProtectionSpell(Receiver Receiver);
    public void printState();
}
