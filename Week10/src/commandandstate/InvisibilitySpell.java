/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandandstate;

/**
 *
 * @author ljclaridge
 */
public class InvisibilitySpell implements ICommand {
    private Receiver receiver;
    public InvisibilitySpell(Receiver receiver) {
        this.receiver = receiver;
    }
    
    @Override
    public void execute() {
        receiver.setVisible(false);
    }

    @Override
    public void undo() {
        receiver.setVisible(true);
    }
    
}
