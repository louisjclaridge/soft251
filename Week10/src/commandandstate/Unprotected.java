/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandandstate;

/**
 *
 * @author ljclaridge
 */
public class Unprotected implements IState {

    @Override
    public void castProtectionSpell(Receiver receiver) {
        System.out.println("Spell Cast! Already unprotected -- doing nothing!");
        receiver.setState(this);
    }

    @Override
    public void toggleProtectionSpell(Receiver receiver) {
        System.out.println("Switching to protected..");
        receiver.setState(new Protected());
    }
    
    @Override
    public void printState() {
        System.out.println("I am unlocked!");
    }
    
     
}
