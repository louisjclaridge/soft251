/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helloworld;

/**
 *
 * @author ljclaridge
 */
public class MyHeight {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        int cm = 100;
        double inches, feet;
        
        inches = cm / 2.54;
        feet = inches / 12;
        inches = inches % 12;
        
        System.out.println(cm + "cm" + " = " + feet + " Feet" + " and " + inches + "inches");
    }
    
}
