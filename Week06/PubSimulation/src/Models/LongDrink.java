/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ljclaridge
 */
public class LongDrink extends Collection3
{
    public void addSpirit()
    {
        System.out.println("Adding Spirit..");
    }
    public void addSimpleGarnish()
    {
        System.out.println("Adding simple garnish..");
    }
    @Override
    public void serveDrink() 
    {
        addIce();
        addSpirit();
        addMixer();
        addSimpleGarnish();
    }
    
}
