/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ljclaridge
 */
public class Cocktail extends Collection3
{
    public void addTwoSpirits()
    {
        System.out.println("Adding two spirits..");
    }
    public void addFancyGarnish()
    {
        System.out.println("Adding fancy garnish..");
    }

    @Override
    public void serveDrink() 
    {
        addIce();
        addTwoSpirits();
        addMixer();
        addFancyGarnish();
    }
    
}
