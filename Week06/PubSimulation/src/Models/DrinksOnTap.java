/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ljclaridge
 */
public class DrinksOnTap extends Collection1 
{
    public void putGlassUnderTap()
    {
        System.out.println("Putting glass under tap..");
    }

    @Override
    public void serveDrink() 
    {
        putGlassUnderTap();
        pourDrinkIntoGlass();
    }
    
}
