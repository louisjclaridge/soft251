/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ljclaridge
 */
public abstract class Collection2 implements DrinkStrategy
{
    public void boilWater()
    {
        System.out.println("Boiling water..");
    }
    public void pourResultIntoCup()
    {
        System.out.println("Pouring result into cup..");
    }
}
