/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ljclaridge
 */
public class Coffee extends Collection2
{
    public void brewCoffee()
    {
        System.out.println("Brewing coffee..");
    }
    public void addMilk()
    {
        System.out.println("Adding milk..");
    }

    @Override
    public void serveDrink() 
    {
        boilWater();
        brewCoffee();
        pourResultIntoCup();
        addMilk();
    }
    
}
