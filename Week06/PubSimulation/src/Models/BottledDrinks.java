/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ljclaridge
 */
public class BottledDrinks extends Collection1 
{
    public void openBottle()
    {
        System.out.println("Opening bottle...");
    }

    @Override
    public void serveDrink() 
    {
        openBottle();
        pourDrinkIntoGlass();
    }
    
}
