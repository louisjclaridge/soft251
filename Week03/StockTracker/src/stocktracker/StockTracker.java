/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker;
import stocktracker.stockdatamodel.*;
import utilities.*;

/**
 *
 * @author ljclaridge
 */
public class StockTracker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StockItem objTestItem1 = new PhysicalStockItem("Starcraft Manual");
        StockItem objTestItem2 = new PhysicalStockItem("Halo 3", 100);
        StockItem objTestItem3 = new ServiceStockItem("Delivery");
        
        //Printing out the objects values
        if(objTestItem1.getItemType() == StockType.PHYSICALITEM)
        {
            System.out.println("Item 1 is a PHYSICAL stock item");
        }
        else
        {
            System.out.println("Item 1 is a SERVICE stock item");
        }
        
        if(objTestItem2.getItemType() == StockType.PHYSICALITEM)
        {
            System.out.println("Item 2 is a PHYSICAL stock item");
        }
        else
        {
            System.out.println("Item 2 is a SERVICE stock item");
        }
        
        if(objTestItem3.getItemType() == StockType.PHYSICALITEM)
        {
            System.out.println("Item 3 is a PHYSICAL stock item");
        }
        else
        {
            System.out.println("Item 3 is a SERVICE stock item");
        }
        
        
        StockItem physicalItem = new PhysicalStockItem();
        StockItem serviceItem = new ServiceStockItem();
        
        AnObserver o = new AnObserver();
        serviceItem.registerObserver(o);
        physicalItem.registerObserver(o);
        
        System.out.println("Changing quantity of the physical stock item");
        physicalItem.setQuantityInStock(5);
        System.out.println("Changing price of the service stock item");
        serviceItem.setCostPrice(5.2);
        
        
        
    }
    
}
