/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker.stockdatamodel;
import java.util.ArrayList;
import utilities.*;
/**
 *
 * @author ljclaridge
 */
public abstract class StockItem implements ISubject
{
    protected String name = "UNKNOWN";
    protected int quantityInStock = 0;
    protected Double sellingPrice = 1000.00;
    protected Double costPrice = 1000.00;
    private ArrayList<IObserver> observers = null;
    
    public Boolean registerObserver(IObserver o)
    {
        Boolean blnAdded = false;
        
        if(o != null)
        {
            if(this.observers == null)
            {
                this.observers = new ArrayList<>();
            
                blnAdded = this.observers.add(o);
            }
        }
            return blnAdded;
    }
    public Boolean removeObserver(IObserver o)
    {
        Boolean blnAdded = false;
        if(o != null)
        {
        blnAdded = this.observers.remove(o);
        }
        return blnAdded;
    }
    public void notifyObservers()
    {
        if(this.observers != null && this.observers.size() > 0)
        {
            for(IObserver currentObserver : this.observers)
            {
                currentObserver.update();
            }
        }
    }
    public StockItem()
    {
        
    }
    public StockItem(String name)
    {
        
    }
    public StockItem(String name, Integer qty)
    {
        
    }
    
    public abstract StockType getItemType();
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name != null && name.isEmpty())
        {
            this.name = name;
            notifyObservers();
        }
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Double sellingPrice) {
        if(sellingPrice != null && sellingPrice >= this.costPrice && sellingPrice >= 0)
        {
            this.sellingPrice = sellingPrice;
            notifyObservers();
        }
    }

    public double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        
        if(costPrice != null && costPrice >= 0)
        {
            this.costPrice = costPrice;
            notifyObservers();
        }
    }
    
    public Boolean isInStock()
    {
        Boolean inStock = false;
        if(this.quantityInStock > 0)
        {
            return true;
        }
        return inStock;
    }
}
