/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bike;

/**
 *
 * @author ljclaridge
 */
public abstract class Bike 
{
    String bikeType = "Unknown Type";
    
    public String getBikeType()
    {
        return bikeType;
    }
    public abstract double cost();
}

