/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Addons;
import Decorator.*;
import Bike.*;
/**
 *
 * @author ljclaridge
 */
public class Helmet extends addonDecorator
{
    public Helmet(Bike bike)
    {
        super(bike);
    }
    @Override
    public String getBikeType()
    {
        return bike.getBikeType() + ", Helmet";
    }
    @Override
    public double cost()
    {
        return bike.cost() + 1.5;
    }
}
