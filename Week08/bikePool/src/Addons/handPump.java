/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Addons;

import Bike.Bike;
import Decorator.*;

/**
 *
 * @author ljclaridge
 */
public class handPump extends addonDecorator
{
    public handPump(Bike bike)
    {
        super(bike);
    }
    @Override
    public String getBikeType()
    {
        return bike.getBikeType() + ", hand pump";
    }
    @Override
    public double cost()
    {
        return bike.cost() + 1.25;
    }    
}
