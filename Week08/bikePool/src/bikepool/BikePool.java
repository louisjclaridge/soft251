/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bikepool;
import Bike.*;
import Addons.*;
/**
 *
 * @author ljclaridge
 */
public class BikePool {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Bike bikeA = new foldingBike();
        Bike bikeB = new Helmet(bikeA);
        Bike bikeC = new handPump(bikeB);
        
        System.out.println(bikeA.getBikeType() + ": " + bikeA.cost());
        System.out.println(bikeB.getBikeType() + ": " + bikeB.cost());
        System.out.println(bikeC.getBikeType() + ": " + bikeC.cost());
    }
    
}
