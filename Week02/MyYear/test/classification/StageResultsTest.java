/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classification;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ljclaridge
 */
public class StageResultsTest {
    private StageResults empty; // will have no credits and no marks
    private StageResults full; // will have 120 credits and marks
    private StageResults halfFull; // will have 60 credits and some marks
    
    public StageResultsTest() 
    {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
     // empty - object that starts with default values
     empty = new StageResults();

     // full - object with 120 credits-worth of marks but no
     // initial stage2Average
     full = new StageResults();
     full.addModuleMark(120, 50.0);

     // halfFull - object with 60 credits worth of marks and
     // no initial stage2Average
     halfFull = new StageResults();
     halfFull.addModuleMark(60, 50.0);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetStage2Average() {
    }

    @Test
    public void testGetTotalCredits() {
    }

    @Test
    public void testGetTotalMarks() {
    }

    @Test
    public void testSetStage2Average() {
    }

    @Test
    public void testIsComplete() {
        System.out.println("Testing isComplete");
        
        //Check that the empty object is 'not complete'
        assertFalse("empty object", empty.isComplete());
       
        //Check full
        assertTrue("Full object", full.isComplete());
        
        //Check half full
        assertFalse("halfFull object", halfFull.isComplete());
        
        
    }

    @Test
    public void testResetValues() {
        System.out.println("Testing Reset Values");
        
        //Set the state of the 'Full' object to zeroes
        full.resetValues();
        
        //Set expected results
        int expIntResult = 0;
        double expDoubleResult = 0.0;
        
        //Now checking each attribute to test that reset has worked
        assertEquals("credits", expIntResult, full.getTotalCredits());
        assertEquals("total", expDoubleResult, full.getTotalMarks(), 0.0);
        
        //Put the 'full' object back to its original state
        full.addModuleMark(120, 50.0);
        
    }

    @Test
    public void testAddModuleMark() {
        
        //Check for 10 credit module with 
        empty.addModuleMark(10, 70.0);
        assertEquals("Credits should equal 10", 10, empty.getTotalCredits());
        assertEquals("Marks should equal 70", 70.0, empty.getTotalMarks(), 0.0);
        empty.resetValues();
       //Check for 20 credit module
       empty.addModuleMark(20, 40.0);
       assertEquals("Credits should equal 20", 20, empty.getTotalCredits());
       assertEquals("Marks should equal 80", 80, empty.getTotalMarks(), 0.0);
       empty.resetValues();
       //Check for 40 credit module
       empty.addModuleMark(40, 80.0);
       assertEquals("Credits should equal 40", 40, empty.getTotalCredits());
       assertEquals("Marks should equal 320", 320, empty.getTotalMarks(), 0.0);
        empty.resetValues();

    }
    

    @Test
    public void testCalculateAverageSoFar() {
        //Testing with no credits and marks
        assertEquals("empty", 0.0, empty.calculateAverageSoFar(), 0.0);
        // Test with 120 credits all at 50%
        assertEquals("full @ 50%", 50, halfFull.calculateAverageSoFar(), 0.0); 
        // Test with 120 credits all at 100%
        full.resetValues();
        full.addModuleMark(120, 100.0);
        assertEquals("full @ 100%", 100.0, full.calculateAverageSoFar(), 0.0); 
        //Test with 120 credits at 43.92%
        full.resetValues();
        full.addModuleMark(120, 43.92);
        assertEquals("full @ 100%", 43.92, full.calculateAverageSoFar(), 0.0); 
        full.resetValues();
        full.addModuleMark(120, 50);
        //Test with 60 credits at 50%
        assertEquals("Half @ 50%", 50, halfFull.calculateAverageSoFar(), 0.0);
        //Test with 60 credits at 100%
        halfFull.resetValues();
        halfFull.addModuleMark(60, 100);
        assertEquals("Half @ 100%", 100, halfFull.calculateAverageSoFar(), 0.0);
        //Test with 60 credits at 64.77%        
        halfFull.resetValues();
        halfFull.addModuleMark(60, 64.77);
        assertEquals("Half @ 64.77%", 64.77, halfFull.calculateAverageSoFar(), 0.);
        
        halfFull.resetValues();
        halfFull.addModuleMark(60, 50);
        
    }

    @Test
    public void testPredictClass() {
        fail("Test not implemented yet");
    }
    
}
